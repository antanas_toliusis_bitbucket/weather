package models;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Antanas on 14.11.21.
 */
public class Weather {

    @SerializedName("name")
    public String cityName;

    @SerializedName("main")
    public Temperature temperature;

    @SerializedName("clouds")
    public Clouds clouds;



    public class Clouds{
        @SerializedName("all")
        public String all;
    }

    public class Temperature{
        @SerializedName("temp_min")
        public Integer temperatureMin;

        @SerializedName("temp_max")
        public Integer temperatureMax;
    }

    public class Wind{
        @SerializedName("speed")
        public Integer speed;
    }

}
