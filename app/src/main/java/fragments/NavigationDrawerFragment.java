package fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.internal.app.ToolbarActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.toliusis.antanas.weather.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antanas on 14.11.20.
 */
public class NavigationDrawerFragment extends Fragment{

    private NavDrawerCallbacks mCallbacks;
    private ListView mListView;
    private DrawerLayout mDrawerLayout;


    public static interface NavDrawerCallbacks{
        public void onItemSelected(int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mListView = (ListView) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
        mListView.setAdapter(new ArrayAdapter<String>(
                getActivity(), R.layout.item_navigation_drawer,
                R.id.label, getLabels()));
        return mListView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle drawerToggle =
                new ActionBarDrawerToggle(getActivity(),mDrawerLayout,
                        R.string.drawer_open,R.string.drawer_closed);

        mDrawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }



    private void selectItem(int position) {
        mDrawerLayout.closeDrawer(getActivity().findViewById(R.id.drawer_fragment));
        mCallbacks.onItemSelected(position);
    }


    private List<String> getLabels(){
        List<String> labels = new ArrayList<String>();
        String [] items = getResources().getStringArray(R.array.navigation_drawer_labels);
        for(String item : items){
            labels.add(item);
        }
        return labels;
    }
}
