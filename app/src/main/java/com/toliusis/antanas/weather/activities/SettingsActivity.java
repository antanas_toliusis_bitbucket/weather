package com.toliusis.antanas.weather.activities;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.toliusis.antanas.weather.R;

public class SettingsActivity extends BaseActivity {

    public static final String PREFS = "com.antanas.toliusis.Weather";
    public static final String KEY_CLOUDS_ON = "KEY_CLOUDS_ON";
    private Button leftButton;
    private Button rightButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        leftButton = (Button) findViewById(R.id.left_button);
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCloudsPref(true);
                setButtonColors();
            }
        });
        rightButton = (Button) findViewById(R.id.right_button);
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCloudsPref(false);
                setButtonColors();
            }
        });
        setButtonColors();
    }


    private void setButtonColors(){
        //if default light is selected
        if(showClouds(this)){
            leftButton.setBackgroundColor(getResources().getColor(android.R.color.holo_orange_dark));
            rightButton.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        }
        else {
            leftButton.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
            rightButton.setBackgroundColor(getResources().getColor(android.R.color.holo_orange_dark));
        }
    }


    private void setCloudsPref(boolean showClouds){
        SharedPreferences settings = getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        settings.edit().putBoolean(KEY_CLOUDS_ON, showClouds).commit();
    }


    public static boolean showClouds(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return settings.getBoolean(KEY_CLOUDS_ON, true);
    }

}
