package com.toliusis.antanas.weather.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import com.toliusis.antanas.weather.R;
import fragments.NavigationDrawerFragment;

/**
 * Created by Antanas on 14.11.20.
 */
public class BaseActivity extends ActionBarActivity implements NavigationDrawerFragment.NavDrawerCallbacks{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ActionBar actionBar= getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onItemSelected(int position) {
        Intent intent = new Intent();

        switch (position){
            case 0 :
                intent.setClass(this, WeatherActivity.class);
                break;
            case 1 :
                intent.setClass(this, IrishWeatherActivity.class);
                break;
            case 2 :
                intent.setClass(this, SettingsActivity.class);
                break;
            case 3 :
                intent.setClass(this, ContactActivity.class);
        }

        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

}
