package com.toliusis.antanas.weather.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.toliusis.antanas.weather.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import models.Weather;

/**
 * Created by Antanas on 14.11.20.
 */
public class WeatherActivity extends BaseActivity {

    private static final String URL_WEATHER = "http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric";
    private static final String DEGREE = "\u00b0";
    private View[] weatherRows = new View[3];
    private View weatherTable;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        weatherTable = findViewById(R.id.weather_table);
        weatherTable.setVisibility(View.INVISIBLE);
        weatherRows[0] = findViewById(R.id.row1);
        weatherRows[1] = findViewById(R.id.row2);
        weatherRows[2] = findViewById(R.id.row3);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setIndeterminate(true);
        new WatherTask().execute();
    }


    private class WatherTask extends AsyncTask<Void,Void, List<Weather>>{
        @Override
        protected List<Weather> doInBackground(Void... params) {
            try {
                URL url = new URL(URL_WEATHER);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                InputStream inputStream = connection.getInputStream();

                BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                StringBuilder responseBuilder = new StringBuilder();
                String inputStr;
                while ((inputStr = streamReader.readLine()) != null){
                    responseBuilder.append(inputStr);
                }
                inputStream.close();

                return parseWeather(new JSONObject(responseBuilder.toString()));
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Weather> weatherList) {
            super.onPostExecute(weatherList);

            if(weatherList != null){
                progressBar.setVisibility(View.GONE);
                weatherTable.setVisibility(View.VISIBLE);
                for(int i = 0; i < weatherList.size(); i++){
                    setRowData(weatherList.get(i), i);
                }
            }
        }
    }


    private List<Weather> parseWeather(JSONObject data) throws JSONException {
        JSONArray weatherData = data.getJSONArray("list");
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Weather>>(){}.getType();
        List<Weather> weatherList = gson.fromJson(weatherData.toString(), listType);
        return weatherList;
    }


    private void setRowData(Weather weather, int index){
        if(weather != null){
            ((TextView)weatherRows[index].findViewById(R.id.data_1)).setText(weather.cityName);
            ((TextView)weatherRows[index].findViewById(R.id.data_2)).setText("min: " + weather.temperature.temperatureMin+ DEGREE);
            ((TextView)weatherRows[index].findViewById(R.id.data_3)).setText("max: " + weather.temperature.temperatureMax+ DEGREE);
            // check if user set clouds off
            if(SettingsActivity.showClouds(this)){
                ((TextView)weatherRows[index].findViewById(R.id.data_4)).setText(weather.clouds.all + " %");
            }
            else {
                ((TextView)weatherRows[index].findViewById(R.id.data_4)).setText("");
            }
        }
    }

}