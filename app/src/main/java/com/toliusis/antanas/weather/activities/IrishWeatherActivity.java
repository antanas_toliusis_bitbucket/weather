package com.toliusis.antanas.weather.activities;

import android.os.Bundle;
import android.webkit.WebView;

import com.toliusis.antanas.weather.R;

/**
 * Created by Antanas on 14.11.20.
 */
public class IrishWeatherActivity extends BaseActivity{

    private static final String NEWS_URL = "http://m.rte.ie/weather/";
    private WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_irish_weather);
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.loadUrl(NEWS_URL);
    }

}
